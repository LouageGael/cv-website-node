const express = require("express");
const bodyParser = require("body-parser");
const app = express();
// use ejs
app.set('view engine', 'ejs');
// body parse (get data from forms etc)
app.use(bodyParser.urlencoded({extended:true}));
//get acces to static files like css files
app.use(express.static("public"));

//  my object for ervaring
const ervaring = [
 {
    plek: "Obelisk, Multimedi",
    jaar: "2021 - 2022",
    omschrijving: ".Net ontwikkelaar met c#"
 },
 {
    plek: "Eriks",
    jaar: "2017 - 2020",
    omschrijving: "Magazijnier"
 },
 {
    plek: "Group-Intro",
    jaar: "2009 - 2010",
    omschrijving: "Metser"
 },
 {
    plek: "Dandoy",
    jaar: "2017 - 2017",
    omschrijving: "Magazijnier"
 },
 {
    plek: "Regina Pacis",
    jaar: "2003 - 2005",
    omschrijving: "Kantoor"
 },
 {
    plek: "Sto",
    jaar: "2012 - 2017",
    omschrijving: "Magazijnier"
 },
 {
    plek: "Campus-Wemmel",
    jaar: "2002 - 2003",
    omschrijving: "Sport"
 },
 {
    plek: "Roba Van Der Rijn",
    jaar: "2011 - 2012",
    omschrijving: "Magazijnier"
 }
];

app.get("/", (req, res) => {
    res.render('main', {mijnObjectErvaring: ervaring});
});

app.listen(3000, () => {
    console.log("server statred on port 3000.");
});