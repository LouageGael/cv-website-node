window.onscroll = function() {scrollFunction()};
let bars = document.querySelectorAll(".ht");
function scrollFunction() {
  if (document.body.scrollTop > 900 || document.documentElement.scrollTop > 900) {
      bars[0].classList.add("html");
      bars[1].classList.add("javascript");
      bars[2].classList.add("css");
      bars[3].classList.add("csharp");
  }
  else {
      bars[0].classList.remove("html");
      bars[1].classList.remove("javascript");
      bars[2].classList.remove("css");
      bars[3].classList.remove("csharp");
  }
}